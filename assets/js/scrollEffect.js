var minLogoWid = 700;
var toggleNav = true;
var navAnimationX = 50;
var fadeAnimationDuration = 1000;

$(function(){

   $("#about-nav").add("#games-nav").add("#web-nav").add("#software-nav").add("#team-nav").add("#contact-nav").add("#games-nav-button").add("#web-nav-button").add("#soft-nav-button").add("#learn-more-button").add("#logo-brand").on("click", function(e){
       if(this.hash != ""){
           e.preventDefault();
           scrollDown(this.hash)
       } 
    });
    
    $("#games-nav2").on('click', function(){
         scrollDown("#products-section")
    });
     $("#web-nav2").on('click', function(){
         scrollDown("#web-section")
    });
     $("#soft-nav2").on('click', function(){
         scrollDown("#software-section")
    });
    
    var wid = window.innerWidth;
    var hei = window.innerHeight;

    
    if(wid>minLogoWid){
        toggleNavbarAndLogo(true);
    }else{
        console.log("haha!");
        toggleNavbarAndLogo(false);
    }

    

    $(window).on("scroll", checkVisibilty);

    $("#customNavbarButton").on('click', function(){
        toggleAnimation();
        toggleNav = !toggleNav;
    });


});

function calculateScrollPercentage(s, d, c){
    return (s / (d-c));
}

function setProgress(amt){
  amt = (amt < 0) ? 0 : (amt > 1) ? 1 : amt;
  document.getElementById("stop1").setAttribute("offset", amt);
  document.getElementById("stop2").setAttribute("offset", amt);
}


function toggleNavbarAndLogo(showLogo){
    if(showLogo){
        $("#mynavbar").fadeOut(fadeAnimationDuration);
        $("#logoPlaceholder").hide().fadeIn(fadeAnimationDuration);
    }else{
        $("#mynavbar").fadeIn(fadeAnimationDuration);
        $("#logoPlaceholder").hide().fadeOut(fadeAnimationDuration);
    }
}

$(window).resize(function(){
    if(window.innerWidth<700){
        $("#mynavbar").css("display", "block");
        $("#logoPlaceholder").hide();
    }else{
        $("#mynavbar").hide();
        $("#logoPlaceholder").css("display", "block");
    }
});


function checkVisibilty() {

    var scrollTop = $(this).scrollTop();
    var d = $(document).height();
    var c = $(window).height();
    console.log(calculateScrollPercentage(scrollTop,d,c));
    setProgress(calculateScrollPercentage(scrollTop,d,c));

    var a1 = ["about-section", "products-section",  "web-section", "software-section", "team-section", "contact-section"];
    var a2 = ["about-nav", "games-nav", "web-nav", "software-nav", "team-nav", "contact-nav"];
    var c = false;

    if(scrollTop==0 && window.innerWidth>700){
        toggleNavbarAndLogo(true);
    }

    if(!elementInViewport2(document.getElementById("myjumbo"))){
        toggleNavbarAndLogo(false);
    }

    for(var i = 0; i < a1.length; i++){
        var b = elementInViewport2(document.getElementById(a1[i]));
   
        if(!c){
            if(b){
               document.getElementById(a2[i]).style.color="red";
               document.getElementById(a2[i]).focus();
               c=true;
             }else{
                document.getElementById(a2[i]).style.color="white";
             }
        }else{
             document.getElementById(a2[i]).style.color="white";
        }
    }
    
    if(scrollTop!=0){
       document.getElementById("mynavbar").style.backgroundColor="black";
    }else{
      document.getElementById("mynavbar").style.backgroundColor="rgba(37,37,37,0.51)";
    }
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
        document.getElementById(a2[4]).style.color="white";
        document.getElementById(a2[5]).style.color="red";
        document.getElementById(a2[5]).focus();
   }
 
}


function scrollDown(hash){
      $('html, body').animate({
				scrollTop: $(hash).offset().top-70
		}, 800);
}


function elementInViewport2(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while (el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top < (window.pageYOffset + window.innerHeight) &&
        left < (window.pageXOffset + window.innerWidth) &&
        (top + height) > window.pageYOffset &&
        (left + width) > window.pageXOffset
    );
}


function toggleAnimation(){
    if(toggleNav){
        var d = 45;
        $(".line2").fadeOut(navAnimationX);
        
        $(".line1").animate(
            {deg: d},{
                duration: navAnimationX,
                step: function(now){
                    $(this).css({transform: 'rotate('+now+'deg)', top: '50%'});
                }
            });

        $(".line3").animate(
            {deg: -d},{
                duration: navAnimationX,
                step: function(now){
                    $(this).css({transform: 'rotate('+now+'deg)', top: '50%'});
                }
            });
    }else{
        var d = 0;
        
        $(".line1").animate(
            {deg: d},{
                duration: navAnimationX,
                step: function(now){
                    $(this).css({transform: 'rotate('+now+'deg)', top: '20%'});
                }
            });
        $(".line2").fadeIn(navAnimationX);
        $(".line3").animate(
            {deg: -d},{
                duration: navAnimationX,
                step: function(now){
                    $(this).css({transform: 'rotate('+now+'deg)', top: '80%'});
                }
            });
    }
}